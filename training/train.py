#!/bin/env python3

import tensorflow as tf
from DataImport import TFRecordToDataset as TF2d
from preprocessing import inception as inception_preprocessing


slim = tf.contrib.slim


def load_batch(dataset, batch_size=32, height=299, width=299, is_training=True):
    """Loads a single batch of data.

    Args:
      dataset: The dataset to load.
      batch_size: The number of images in the batch.
      height: The size of each image after preprocessing.
      width: The size of each image after preprocessing.
      is_training: Whether or not we're currently training or evaluating.

    Returns:
      images: A Tensor of size [batch_size, height, width, 3], image samples that have been preprocessed.
      images_raw: A Tensor of size [batch_size, height, width, 3], image samples that can be used for visualization.
      labels: A Tensor of size [batch_size], whose values range between 0 and dataset.num_classes.
    """
    data_provider = slim.dataset_data_provider.DatasetDataProvider(
        dataset, common_queue_capacity=32,
        common_queue_min=8)
    image_raw, label = data_provider.get(['image', 'label'])

    # Preprocess image for usage by Inception.
    image = inception_preprocessing.preprocess_image(image_raw, height, width, is_training=is_training)

    # image = tf.image.resize_image_with_crop_or_pad(
    #     image_raw,
    #     height,
    #     width
    # )

    # Preprocess the image for display purposes.
    # image_raw = tf.expand_dims(image_raw, 0)
    # image_raw = tf.image.resize_images(image_raw, [height, width])
    # image_raw = tf.squeeze(image_raw)

    # Batch it up.
    # images, images_raw, labels = tf.train.batch(
    #     [image, image_raw, label],
    #     batch_size=batch_size,
    #     num_threads=1,
    #     capacity=2 * batch_size,
    #     enqueue_many=True)

    images, labels = tf.train.batch(
        [image, label],
        batch_size=batch_size,
        num_threads=1,
        capacity=5 * batch_size)

    #  labels = slim.one_hot_encoding(labels, dataset.num_classes)
    #  batch_queue = slim.prefetch_queue.prefetch_queue([images, labels], capacity=2 * )

    return images, labels


def train(model, batch_size=5, num_epoch=40):
    train_log_dir = "train_log_dir"
    datasets = {
        "training": ["TFDatasets/flowers/flowers-training.tfrecords"],
        "validation": ["TFDatasets/flowers/flowers-validation.tfrecords"],
    }

    if not tf.gfile.Exists(train_log_dir):
        tf.gfile.MakeDirs(train_log_dir)

    with tf.Graph().as_default():
        tf.logging.set_verbosity(tf.logging.DEBUG)

        training_dataset = TF2d.get_split(datasets["training"][0])
        images, labels = load_batch(training_dataset, batch_size=batch_size, height=300, width=300, is_training=True)

        logits = model(images, num_classes=training_dataset.num_classes, is_training=True)
        predictions = tf.argmax(logits, 1)

        # Define the metrics:
        names_to_values, names_to_updates = slim.metrics.aggregate_metric_map({
            "eval/Accuracy": slim.metrics.streaming_accuracy(predictions, labels),
            "eval/Recall@5": slim.metrics.streaming_sparse_recall_at_k(logits, labels, 5),
            "eval/Recall@1": slim.metrics.streaming_sparse_recall_at_k(logits, labels, 1)
        })

        # Specify the loss function:
        one_hot_labels = slim.one_hot_encoding(labels, training_dataset.num_classes)
        tf.losses.softmax_cross_entropy(one_hot_labels, logits)
        total_loss = tf.losses.get_total_loss()

        # Create some summaries to visualize the training process:
        tf.summary.scalar("losses/Total_Loss", total_loss)

        # Specify the optimizer and create the train op:
        optimizer = tf.train.AdamOptimizer(learning_rate=0.01)
        train_op = slim.learning.create_train_op(total_loss, optimizer)

        # Run the training:
        final_loss = slim.learning.train(
            train_op,
            logdir=train_log_dir,
            number_of_steps=num_epoch,  # For speed, we just do 1 epoch
            save_summaries_secs=1)

        print("Finished training. Final batch loss %d" % final_loss)

        print("Running evaluation Loop...", end="")
        checkpoint_path = tf.train.latest_checkpoint(train_log_dir)
        metric_values = slim.evaluation.evaluate_once(
            master="",
            checkpoint_path=checkpoint_path,
            logdir=train_log_dir,
            eval_op=names_to_updates.values(),
            final_op=names_to_values.values())
        print("Done")

        names_to_values = dict(zip(names_to_values.keys(), metric_values))
        for name in names_to_values:
            print("{0}: {1}".format(name, names_to_values[name]))