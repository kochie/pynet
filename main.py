#!/usr/bin/env python3

import argparse
import os

import tensorflow as tf

from data_input import *
from models import alexnet, test_cnn

import training.train as train

# TODO: download MNIST
# TODO: output MNIST
# TODO: create alexnet
# TODO: train alexnet
# TODO: test alexnet
# TODO: validate alexnet
# TODO: save alexnet
# TODO: integrate TensorBoard


def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument("-d", "--dataset",
                        help="Dataset Name (CIFAR-10 | CIFAR-100 | MNIST)",
                        default="mnist", dest="dataset")

    parser.add_argument("-m", "--model",
                        help="Model Name (AlexNet | BinaryNet | " +
                        "TernaryNet | XNOR-Net)",
                        default="alexnet", dest="model")

    parser.add_argument("-b", "--batchsize",
                        help="Number of images to train per batch",
                        default=5, dest="batch_size", nargs=1, type=int)

    parser.add_argument("-e", "--epoch",
                        help="Number of batches to train.",
                        default=100, dest="num_epoch", nargs=1, type=int)

    return parser.parse_args()
#
#
# def read_data(dataset):
#     if (dataset.lower() == "cifar-10"):
#         return cifar.read_cifar10()
#     elif (dataset.lower() == "cifar-100"):
#         return cifar.read_cifar100()
#     elif (dataset.lower() == "mnist"):
#         return mnist.read_mnist()
#     else:
#         print("Argument not supported")
#         os.exit(1)
#
#
# def read_model(model):
#     if (model.lower() == "alexnet"):
#         return build_alexnet(inputs, outputs)
#     else:
#         print("Argument not supported")
#         os.exit(1)
#
#
# def main():
#     args = parseArgs()
#     data = read_data(args.dataset)
#     model, end_points = read_model(args.model, 28 * 28, 10)
#     print(model)
#     train(model, data)


def alex_net_setup(inputs, num_classes=1000, is_training=True, dropout_keep_prob=0.5, spatial_squeeze=True,
                   scope='alexnet_v2'):
    with slim.arg_scope(alexnet.alexnet_v2_arg_scope()):
        return alexnet.alexnet_v2(inputs, num_classes=num_classes, is_training=is_training, dropout_keep_prob=dropout_keep_prob,
                                  spatial_squeeze=spatial_squeeze, scope=scope)


if __name__ == "__main__":
    slim = tf.contrib.slim
    args = parse_args()
    train.train(test_cnn.my_cnn, args.batch_size[0], args.num_epoch[0])
