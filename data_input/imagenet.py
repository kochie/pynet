import tensorflow as tf
from datasets import imagenet

slim = tf.contrib.slim

# Selects the 'validation' dataset.
dataset = imagenet.get_split('validation', "/FastStorage/ImageNet")

# Creates a TF-Slim DataProvider which reads the dataset in the background
# during both training and testing.
provider = slim.dataset_data_provider.DatasetDataProvider(dataset)
[image, label] = provider.get(['image', 'label'])
