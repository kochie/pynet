from datetime import datetime

import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data

from alexnet import *

# Set up TensorBoard

now = datetime.utcnow().strftime("%Y%m%d%H%M%S")
root_dir = "tf_logs"
logdir = "{0}/run-{1}-{2}".format(root_dir, "MNIST-ALEXNET", now)

# Import data section

mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)

# Construction section
inputs = tf.placeholder(tf.float32, shape=(28, 28, 1), name="Input")
outputs = tf.placeholder(tf.float32, shape=(10, 1), name="Output")

net = alexnet_v2(inputs, outputs)
optimizer = tf.train.AdamOptimizer(learning_rate=0.001)
training_op = optimizer.minimize(net)

# Execution section
# TODO: training_op
# TODO: batch

with tf.Session() as sess:
    sess.run(init)

    for epoch in range(n_epochs):
        for batch_index in range(n_batches):
            X_batch, y_batch = mnist.train.next_batch(batch_size)
            sess.run(training_op, feed_dict={X: X_batch, y: y_batch})
            if batch_index % 10 == 0:
                summary_str = mse_summary.eval(
                    feed_dict={X: X_batch, y: y_batch}
                )
                step = epoch * n_batches + batch_index
                file_writer.add_summary(summary_str, step)
        if epoch % 100 == 0:
            save_path = saver.save(sess, "/tmp/my_model.ckpt")
            print("Epoch", epoch)

    save_path = saver.save(sess, "/tmp/my_model_final.ckpt")

file_writer.close()
