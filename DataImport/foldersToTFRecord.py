#!/bin/env python3

import argparse
import sys
import os
import re
import random
import time

import json
import tensorflow as tf

from typing import List, Dict


def read_arguments():
    """
    readArguments is a helper function to define all arguments sent to the script.
    """
    parser = argparse.ArgumentParser(description="A helper module to convert a directory of images into TFRecords.")
    parser.add_argument(
        "directories",
        nargs="*",
        help="List of directories to look into.",
        default=sys.stdin,
        type=str,
        )

    parser.add_argument(
        "datasetName",
        nargs=1,
        help="The name of the dataset",
        default=os.path.basename(os.getcwd()),
        type=str
        )

    parser.add_argument(
        "directoryName",
        nargs=1,
        help="Location to save the dataset to.",
        default=os.getcwd(),
        type=str
        )

    return parser.parse_args()


def int64_feature(value):
    """
    Return a feature of type int64, populated with value.
    """
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def bytes_feature(value):
    """
    Return a feature of type bytes, populated with value.
    """
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def image_to_tfexample(image_data, image_format, height, width, depth, class_id):
    """
    Create a tf.train.Example Object from the image data.

    :argument image_data: The image binary data represented as a string.
    :argument image_format: A byte array representing the image format.
    :argument height: The height of the image in pixels.
    :argument width: The width of the image in pixels.
    :argument depth: The depth of the image in pixels. (i.e. the colour layers.)
    :argument class_id: An integer representing the class.
    :return: A tf.train.Example Object.
    """
    return tf.train.Example(features=tf.train.Features(feature={
        "image/encoded": bytes_feature(image_data),
        "image/format": bytes_feature(image_format),
        "image/class/label": int64_feature(class_id),
        "image/height": int64_feature(height),
        "image/width": int64_feature(width),
        "image/depth": int64_feature(depth)
        }))


def convert_to(
        image: tf.image,
        label: object,
        writer: tf.python_io.TFRecordWriter,
        image_format: bytes = b"PNG",
        rows: int = 100,
        cols: int = 100,
        depth: int = 100
) -> None:
    """
    Convert an image with its metadata to a tf
    
    :param image: A tensor
    :param label: 
    :param writer:
    :param image_format: 
    :return: 
    """
    #rows = image.shape[0]
    #cols = image.shape[1]
    #depth = image.shape[2]
    image_raw = image
    example = image_to_tfexample(image_raw, image_format, rows, cols, depth, label)
    writer.write(example.SerializeToString())


def make_filename_queue(directory_list: List[str]) -> Dict[str, List[str]]:
    """
    makeFilenameQueue finds all the images in the list of directories and returns a list of the fully qualified
    filenames.

    :argument directory_list:
    :return: A queue of filenames.
    :rtype: Dict[str, List[str]]
    """
    directory_list = [os.path.abspath(x) for x in directory_list]
    end_match = re.compile(r"\.png$|\.jpe?g$|\.gif$|\.bmp$")
    images = dict()

    # Go through all the directories that are listed in the input and output files.
    while directory_list:
        current_directory = directory_list.pop()
        for root, dirs, files in os.walk(current_directory):
            label = os.path.basename(root)
            images[label] = list()
            for name in files:
                filename = os.path.join(root, name)
                if end_match.search(filename):
                    images[label].append(filename)
                    print("Adding image to queue ~ label: {0} -- filename: {1}".format(label, filename))
                else:
                    pass

    return images


def randomise_image_names(image_names, validation_percentage=0.25):
    """
    Given a list of image names, the method will randomise and split into validation and training set.

    :param image_names: An array of filenames.
    :param validation_percentage: The percentage of files that should be included in the validation set.
    :return: The training and validation sets.
    """
    # Divide into train and test:
    num_validations = int(len(image_names)*validation_percentage)
    random.seed(int(time.time()))
    random.shuffle(image_names)
    training_filenames = image_names[num_validations:]
    validation_filenames = image_names[:num_validations]

    return training_filenames, validation_filenames


def write_label_file(labels_to_class_names, dataset_dir, filename="labels.txt"):
    """Writes a file with the list of class names.
    :argument labels_to_class_names:
    :argument dataset_dir:
    :argument filename:
    labels_to_class_names: A map of (integer) labels to class names.
    dataset_dir: The directory in which the labels file should be written.
    filename: The filename where the class names are written.
    """
    labels_filename = os.path.join(dataset_dir, filename)
    with tf.gfile.Open(labels_filename, 'w') as f:
        for label, class_name in labels_to_class_names.items():
            f.write("{0}:{1}\n".format(label, class_name))


def batch_write_images(set_type, filenames, dataset_name, img, directory, class_names_to_ids):
    """
    batchWriteImages will write a set to a filename.
    """
    print("\n\nMaking {0} set".format(set_type))
    filename = os.path.join(directory, "{0}-{1}.tfrecords".format(dataset_name, set_type))
    print('Writing', filename)
    with tf.python_io.TFRecordWriter(filename) as writer:
        for image_file in filenames:  # length of your filename list
            label = os.path.split(os.path.split(image_file)[0])[-1]
            _, image_format = os.path.splitext(image_file)
            image = img.eval()  # here is your image Tensor :)
            print("Adding file to record: {0}".format(image_file))
            convert_to(tf.gfile.FastGFile(image_file, 'rb').read(), class_names_to_ids[label], writer, str.encode(image_format[1:].upper()), image.shape[0], image.shape[1], image.shape[2])


def images_to_tfrecord(directory_list: List[str], dataset_name: str, directory: str = os.getcwd()):
    """
    imagesToTFRecord will recursively pass through a list of directories and add images it finds to the TFRecords.
    
    Two TFRecords will be created [train, validation] in the output directory. These records will be created using
    random files from the input directories.
    :argument directory_list: A list of directories to search.
    :argument dataset_name: The name to give the dataset.
    :argument directory: The output directory.
    """

    if not directory_list:
        raise ValueError("No directories have been listed!")
    else:
        pass

    reader = tf.WholeFileReader()
    images = make_filename_queue(directory_list)
    image_names = list()
    class_names = [k for k, v in images.items() if v]
    class_names_to_ids = dict(zip(class_names, range(len(class_names))))
    print(class_names_to_ids)

    for value in images.values():
        image_names.extend(value)

    training_filenames, validation_filenames = randomise_image_names(image_names)
    filename_queue = tf.train.string_input_producer(image_names)  # List of files to read
    key, value = reader.read(filename_queue)

    if os.path.exists(directory):
        pass
    else:
        os.makedirs(directory)

    directory = os.path.abspath(directory)
    img = tf.image.decode_image(value)
    init = tf.global_variables_initializer()

    with tf.Session() as sess:
        sess.run(init)

        # Start populating the filename queue.
        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(coord=coord)

        batch_write_images("training", training_filenames, dataset_name, img, directory, class_names_to_ids)
        batch_write_images("validation", validation_filenames, dataset_name, img, directory, class_names_to_ids)

        coord.request_stop()
        coord.join(threads)

    print("Exporting sample sizes...", end="")
    write_sample_file({
        "training": len(training_filenames),
        "validation": len(validation_filenames)
    }, directory)
    print("Done")

    print("Exporting labels...", end="")
    write_label_file(class_names_to_ids, directory)
    print("Done")


def write_sample_file(sample_size, directory):
    with open(os.path.join(directory, "sample_size.json"), "w") as sample_size_file:
        json.dump(sample_size, sample_size_file, indent=4)


if __name__ == "__main__":
    args = read_arguments()
    images_to_tfrecord(args.directories, args.datasetName[0], args.directoryName[0])
