#!/bin/env python3
import urllib.request
import os
import tarfile
import sys
import time


def prompt_user(message, default_yes=True):
    options = "Y/n" if default_yes else "y/N"
    response = input("{0} [{1}]: ".format(message, options))
    if response == "":
        return default_yes
    else:
        return True if (response == "Y" or response == "y") else False


def convert_bytes(num):
    """
    this function will convert bytes to MB.... GB... etc
    """
    for x in ['bytes', 'KB', 'MB', 'GB', 'TB']:
        if num < 1024.0:
            return "%3.1f %s" % (num, x)
        num /= 1024.0


def dl_progress(count, block_size, total_size):
    sys.stdout.write("\rDownloading: {0:.2%} -- {1}".format(
        (count * block_size / total_size), convert_bytes(count * block_size)))
    sys.stdout.flush()


def downloadFlowers():
    directory = "Datasets"
    dataset_name = "flower_photos"
    if os.path.exists(directory):
        pass
    else:
        os.mkdir(directory)

    filename = os.path.join(directory, dataset_name + ".tgz")
    if os.path.exists(filename):
        print("The file has already been downloaded!")
    else:
        start = time.time()
        print()
        urllib.request.urlretrieve("http://download.tensorflow.org/example_images/flower_photos.tgz", filename, reporthook=dl_progress)
        print("\nDownload Completed in {0} seconds!".format(int(time.time()-start)))

    dataset_dir = directory
    if os.path.exists(dataset_dir):
        pass
    else:
        os.mkdir(dataset_dir)

    tf = tarfile.open(filename)
    print("Extracting Tarball...", end="")
    tf.extractall(dataset_dir)
    print("Done!")

    if prompt_user("Do you want to delete the tarfile?"):
        os.remove(filename)
    else:
        pass


def downloadDatasets():
    pass


if __name__ == "__main__":
    downloadFlowers()
