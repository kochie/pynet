#!/bin/env python3

import time
import os

import json
import tensorflow as tf

from typing import *

slim = tf.contrib.slim


def tf_record_to_dataset(tfrecord_filenames: Dict[str, List[str]], batch_size: int = 50):
    """
    Creates datasets based on the keys given.

    :param batch_size: The batch size.
    :type tfrecord_filenames: A dictionary containing the list of filenames.
    :rtype: void
    :return: void
    """

    # Assert some things to make sure the files given are correct.
    assert "training" in tfrecord_filenames
    assert "validation" in tfrecord_filenames

    tf.set_random_seed(int(time.time()))
    filenames = tf.placeholder(tf.string, shape=[None])
    dataset = tf.contrib.data.TFRecordDataset(filenames)
    # dataset = dataset.map(...)  # Parse the record into tensors.
    dataset = dataset.shuffle(buffer_size=10000)
    dataset = dataset.repeat()  # Repeat the input indefinitely.
    dataset = dataset.batch(batch_size)
    iterator = dataset.make_initializable_iterator()

    with tf.Session() as sess:
        # Initialize `iterator` with training data.
        training_filenames = tfrecord_filenames["training"]
        sess.run(iterator.initializer, feed_dict={filenames: training_filenames})
        for _ in range(1):
            print(sess.run(iterator.get_next()))

        # Initialize `iterator` with validation data.
        validation_filenames = tfrecord_filenames["validation"]
        sess.run(iterator.initializer, feed_dict={filenames: validation_filenames})


if __name__ == "__main__":
    tfrecord_datasets = {
        "training": ["TFDatasets/flowers/flowers-training.tfrecords"],
        "validation": ["TFDatasets/flowers/flowers-validation.tfrecords"]
    }
    tf_record_to_dataset(tfrecord_datasets)


def get_split(dataset: str) -> slim.dataset.Dataset:
    # Copyright 2016 The TensorFlow Authors. All Rights Reserved.
    #
    # Licensed under the Apache License, Version 2.0 (the "License");
    # you may not use this file except in compliance with the License.
    # You may obtain a copy of the License at
    #
    # http://www.apache.org/licenses/LICENSE-2.0
    #
    # Unless required by applicable law or agreed to in writing, software
    # distributed under the License is distributed on an "AS IS" BASIS,
    # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    # See the License for the specific language governing permissions and
    # limitations under the License.
    # ==============================================================================
    """Provides data for the flowers dataset.
    The dataset scripts used to create the dataset can be found at:
    tensorflow/models/research/slim/datasets/download_and_convert_flowers.py
    """

    """Gets a dataset tuple with instructions for reading flowers.
    Args:
      split_name: A train/validation split name.
      dataset_dir: The base directory of the dataset sources.
      file_pattern: The file pattern to use when matching the dataset sources.
        It is assumed that the pattern contains a '%s' string so that the split
        name can be inserted.
      reader: The TensorFlow reader type.
    Returns:
      A `Dataset` namedtuple.
    Raises:
      ValueError: if `split_name` is not a valid train/validation split.
    """

    # SPLITS_TO_SIZES = {'train': 3320, 'validation': 350}

    # _NUM_CLASSES = 5

    items_to_descriptions = {
        'image': 'A color image of varying size.',
        'label': 'A single integer between 0 and 4',
    }

    reader = tf.TFRecordReader

    keys_to_features = {
        "image/encoded": tf.FixedLenFeature((), tf.string, default_value=""),
        "image/format": tf.FixedLenFeature((), tf.string, default_value="png"),
        "image/class/label": tf.FixedLenFeature([], tf.int64, default_value=tf.zeros([], dtype=tf.int64))
    }

    items_to_handlers = {
        "image": slim.tfexample_decoder.Image(),
        "label": slim.tfexample_decoder.Tensor("image/class/label")
    }

    decoder = slim.tfexample_decoder.TFExampleDecoder(keys_to_features, items_to_handlers)

    dataset_dir = os.path.dirname(dataset)

    sample_size = None
    number_of_classes = None
    labels_to_names = None
    if has_labels(dataset_dir):
        sample_size = get_sample_size(dataset_dir)
        number_of_classes = get_classes(dataset_dir)
        labels_to_names = read_label_file(dataset_dir)

    return slim.dataset.Dataset(
        data_sources=dataset,
        reader=reader,
        decoder=decoder,
        num_samples=sample_size,
        items_to_descriptions=items_to_descriptions,
        num_classes=number_of_classes,
        labels_to_names=labels_to_names
    )


def get_sample_size(dataset_dir: str, dataset: str="training") -> int:
    with open(os.path.join(dataset_dir, "sample_size.json"), "r") as sample_file:
        return int(json.load(sample_file)[dataset])


def get_classes(dataset_dir: str) -> int:
    with open(os.path.join(dataset_dir, "labels.txt")) as labels:
        return len(labels.readlines())


def has_labels(dataset_dir, filename="labels.txt"):
    """Specifies whether or not the dataset directory contains a label map file.
    Args:
    dataset_dir: The directory in which the labels file is found.
    filename: The filename where the class names are written.
    Returns:
    `True` if the labels file exists and `False` otherwise.
    """
    return tf.gfile.Exists(os.path.join(dataset_dir, filename))


def read_label_file(dataset_dir, filename="labels.txt"):
    """Reads the labels file and returns a mapping from ID to class name.
    Args:
    dataset_dir: The directory in which the labels file is found.
    filename: The filename where the class names are written.
    Returns:
    A map from a label (integer) to class name.
    """
    labels_filename = os.path.join(dataset_dir, filename)
    with tf.gfile.Open(labels_filename, 'rb') as f:
        lines = f.read().decode()
    lines = lines.split('\n')
    lines = filter(None, lines)

    labels_to_class_names = {}
    for line in lines:
        index = line.index(':')
        labels_to_class_names[line[:index]] = int(line[index+1:])
    return labels_to_class_names
