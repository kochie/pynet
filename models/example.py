import tensorflow as tf
import numpy as np
import random

from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)

sess = tf.InteractiveSession()

learn_rate = 0.0001
momentum = 0.99
optimizer = tf.train.MomentumOptimizer(learn_rate,momentum)

def quant2(tsor) :
    return tf.fake_quant_with_min_max_args(tf.nn.tanh(tsor)+1,0,510)-1

def quant3(tsor) :
    return tf.fake_quant_with_min_max_args(tf.nn.tanh(tsor),-1,254)

x=tf.placeholder(tf.float32,[None,784])
xq = quant2(x-0.0001)
W1 = tf.Variable(tf.truncated_normal([784,20],stddev=1.0))
W1q = quant3(W1)
b1 = tf.Variable(tf.zeros([20]))

hidden = quant2(tf.matmul(xq,W1q)+b1)

W2 = tf.Variable(tf.truncated_normal([20,10],stddev=1.0))
W2q = quant3(W2)
b2 = tf.Variable(tf.zeros([10]))

s=tf.Variable(-3.0,dtype=tf.float32)
sce=tf.exp(s)

y = tf.nn.softmax(sce*(tf.matmul(hidden,W2q)+b2))

y_ = tf.placeholder(tf.float32,[None,10])

cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y), reduction_indices=[1]))
Decay_Factor=0.0005
weightreg = (tf.reduce_sum(tf.square(W1)) + tf.reduce_sum(tf.square(W2))) * Decay_Factor
loss = cross_entropy + weightreg


def trainer(lr):
    return tf.train.GradientDescentOptimizer(lr).minimize(loss)

trainers=[trainer(1.0),trainer(0.5), trainer(0.2), trainer(0.1), trainer(0.05)]

tf.global_variables_initializer().run()

correct_prediction = tf.equal(tf.argmax(y,1), tf.argmax(y_,1))

accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
print("accuracy = ", sess.run(accuracy, feed_dict={x: mnist.test.images, y_: mnist.test.labels}))

for t in trainers:
    for _ in range(500):
        batch_xs, batch_ys = mnist.train.next_batch(100)
        sess.run(t, feed_dict={x: batch_xs, y_: batch_ys})
        print('accuracy = ', sess.run(accuracy, feed_dict={x: mnist.test.images, y_: mnist.test.labels}))


print("loss = ", sess.run(cross_entropy, feed_dict={x: mnist.test.images, y_: mnist.test.labels}))
print('Weights = [', min([min([p for p in r]) for r in sess.run(W)]),', ', max([max([p for p in r]) for r in sess.run(W)]),']')
qW1 = sess.run(W1q)
print('Non zero W1q = ', sum([sum(v!=0 for v in r) for r in qW1]))
qw2 = sess.run(W2q)
print('Non zero W2q = ', sum([sum(v!=0 for v in r) for r in qw2]))

print('s = ', sess.run(s))
print('b = [', min(sess.run(b)),', ',max(sess.run(b)),']')
