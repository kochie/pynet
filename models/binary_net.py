import tensorflow as tf
slim = tf.contrib.slim


def trunc_normal(std_dev: float) -> tf.truncated_normal_initializer:
    """

    :type std_dev: float
    :return: A truncated normal distribution, with a mean of zero and a standard deviation of stddev.
    :rtype: tf.truncated_normal_initializer
    """
    return tf.truncated_normal_initializer(0.0, std_dev)


def binary_net_weights(inputs,
                       num_classes=1000,
                       is_training=True,
                       dropout_keep_prob=0.5,
                       spatial_squeeze=True,
                       scope='binary_net_weights'):
    with tf.variable_scope(scope, 'binary_net_weights', [inputs]) as sc:
        end_points_collection = sc.name + '_end_points'
        # Collect outputs for conv2d, fully_connected and max_pool2d.
        with slim.arg_scope([
                slim.conv2d, slim.fully_connected, slim.max_pool2d
        ],
                outputs_collections=[end_points_collection]):

            net = slim.conv2d(inputs, 64, [11, 11], 4, padding='VALID',
                              scope='conv1')
            net = slim.max_pool2d(net, [3, 3], 2, scope='pool1')
            net = slim.conv2d(net, 192, [5, 5], scope='conv2')
            net = slim.conv2d(net, 384, [3, 3], scope='conv3')
            net = slim.max_pool2d(net, [3, 3], 2, scope='pool2')
            net = slim.conv2d(net, 384, [3, 3], scope='conv4')
            net = slim.conv2d(net, 256, [3, 3], scope='conv5')
            net = slim.max_pool2d(net, [3, 3], 2, scope='pool5')

        # Use conv2d instead of fully_connected layers.
        with slim.arg_scope([slim.conv2d],
                            weights_initializer=trunc_normal(0.005),
                            biases_initializer=tf.constant_initializer(0.1)):

            net = slim.conv2d(net, 4096, [5, 5], padding='VALID',
                              scope='fc6')
            net = slim.dropout(net, dropout_keep_prob, is_training=is_training,
                               scope='dropout6')
            net = slim.conv2d(net, 4096, [1, 1], scope='fc7')
            net = slim.dropout(net, dropout_keep_prob, is_training=is_training,
                               scope='dropout7')
            net = slim.conv2d(net, num_classes, [1, 1],
                              activation_fn=None,
                              normalizer_fn=None,
                              biases_initializer=tf.zeros_initializer(),
                              scope='fc8')

        # Convert end_points_collection into a end_point dict.
        end_points = slim.utils.convert_collection_to_dict(
            end_points_collection)
        if spatial_squeeze:
            net = tf.squeeze(net, [1, 2], name='fc8/squeezed')
            end_points[sc.name + '/fc8'] = net
        return net, end_points


def binary_net_activations(inputs,
                           num_classes=1000,
                           is_training=True,
                           dropout_keep_prob=0.5,
                           spatial_squeeze=True,
                           scope='binary_net_activations'):
    with tf.variable_scope(scope, 'binary_net_activations', [inputs]) as sc:
        end_points_collection = sc.name + 'binary_net_activations'
        # Collect outputs for conv2d, fully_connected and max_pool2d.
        with slim.arg_scope([
            slim.conv2d, slim.fully_connected, slim.max_pool2d
        ],
                outputs_collections=[end_points_collection]):

            net = slim.conv2d(inputs, 64, [11, 11], 4, padding='VALID',
                              scope='conv1')
            net = slim.max_pool2d(net, [3, 3], 2, scope='pool1')
            net = slim.conv2d(net, 192, [5, 5], scope='conv2')
            net = slim.conv2d(net, 384, [3, 3], scope='conv3')
            net = slim.max_pool2d(net, [3, 3], 2, scope='pool2')
            net = slim.conv2d(net, 384, [3, 3], scope='conv4')
            net = slim.conv2d(net, 256, [3, 3], scope='conv5')
            net = slim.max_pool2d(net, [3, 3], 2, scope='pool5')

        # Use conv2d instead of fully_connected layers.
        with slim.arg_scope([slim.conv2d],
                            weights_initializer=trunc_normal(0.005),
                            biases_initializer=tf.constant_initializer(0.1)):

            net = slim.conv2d(net, 4096, [5, 5], padding='VALID',
                              scope='fc6')
            net = slim.dropout(net, dropout_keep_prob, is_training=is_training,
                               scope='dropout6')
            net = slim.conv2d(net, 4096, [1, 1], scope='fc7')
            net = slim.dropout(net, dropout_keep_prob, is_training=is_training,
                               scope='dropout7')
            net = slim.conv2d(net, num_classes, [1, 1],
                              activation_fn=None,
                              normalizer_fn=None,
                              biases_initializer=tf.zeros_initializer(),
                              scope='fc8')

        # Convert end_points_collection into a end_point dict.
        end_points = slim.utils.convert_collection_to_dict(
            end_points_collection)
        if spatial_squeeze:
            net = tf.squeeze(net, [1, 2], name='fc8/squeezed')
            end_points[sc.name + '/fc8'] = net
        return net, end_points


def binary_net_weights_activations(inputs,
                                   num_classes=1000,
                                   is_training=True,
                                   dropout_keep_prob=0.5,
                                   spatial_squeeze=True,
                                   scope='binary_net_weights_activations'):
    with tf.variable_scope(scope,
                           'binary_net_weights_activations',
                           [inputs]) as sc:
        end_points_collection = sc.name + '_end_points'
        # Collect outputs for conv2d, fully_connected and max_pool2d.
        with slim.arg_scope(
            [slim.conv2d, slim.fully_connected, slim.max_pool2d],
                outputs_collections=[end_points_collection]):

            net = slim.conv2d(inputs, 64, [11, 11], 4, padding='VALID',
                              scope='conv1')
            net = slim.max_pool2d(net, [3, 3], 2, scope='pool1')
            net = slim.conv2d(net, 192, [5, 5], scope='conv2')
            net = slim.conv2d(net, 384, [3, 3], scope='conv3')
            net = slim.max_pool2d(net, [3, 3], 2, scope='pool2')
            net = slim.conv2d(net, 384, [3, 3], scope='conv4')
            net = slim.conv2d(net, 256, [3, 3], scope='conv5')
            net = slim.max_pool2d(net, [3, 3], 2, scope='pool5')

        # Use conv2d instead of fully_connected layers.
        with slim.arg_scope([slim.conv2d],
                            weights_initializer=trunc_normal(0.005),
                            biases_initializer=tf.constant_initializer(0.1)):

            net = slim.conv2d(net, 4096, [5, 5], padding='VALID',
                              scope='fc6')
            net = slim.dropout(net, dropout_keep_prob, is_training=is_training,
                               scope='dropout6')
            net = slim.conv2d(net, 4096, [1, 1], scope='fc7')
            net = slim.dropout(net, dropout_keep_prob, is_training=is_training,
                               scope='dropout7')
            net = slim.conv2d(net, num_classes, [1, 1],
                              activation_fn=None,
                              normalizer_fn=None,
                              biases_initializer=tf.zeros_initializer(),
                              scope='fc8')

        # Convert end_points_collection into a end_point dict.
        end_points = slim.utils.convert_collection_to_dict(
            end_points_collection)
        if spatial_squeeze:
            net = tf.squeeze(net, [1, 2], name='fc8/squeezed')
            end_points[sc.name + '/fc8'] = net
        return net, end_points
