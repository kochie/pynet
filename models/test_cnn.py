import tensorflow as tf

slim = tf.contrib.slim


def my_cnn(images, num_classes, is_training):  # is_training is not used...
    with slim.arg_scope([slim.max_pool2d], kernel_size=3, stride=2):
        net = tf.to_float(images)
        net = slim.conv2d(net, 64, 5)
        net = slim.max_pool2d(net)
        net = slim.conv2d(net, 64, [5, 5])
        net = slim.max_pool2d(net)
        net = slim.flatten(net)
        net = slim.fully_connected(net, 192)
        net = slim.fully_connected(net, num_classes, activation_fn=None)
        net = tf.nn.softmax(net)
        return net

