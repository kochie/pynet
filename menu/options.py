import climenu

@climenu.group(title="Change Options")
def change_options():
    # TODO: Call the build scripts here!
    print('built the packages!')

@change_options.group(title="Change Dataset")
def change_dataset():
    print("Model Changed")

@change_dataset.menu(title="MNIST")
def select_MNIST():
    print("MNIST Chosen")

@change_dataset.menu(title="CIFAR-10")
def select_CIFAR_10():
    print("CIFAR-10 Chosen")

@change_options.group(title="Change Model")
def change_model():
    print("Model Changed")

@change_model.menu(title="AlexNet")
def select_AlexNet():
    print("AlexNet Chosen")

@change_model.menu(title="BinaryNet")
def select_BinaryNet():
    print("BinaryNet Chosen")
